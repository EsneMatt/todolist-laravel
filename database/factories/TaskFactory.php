<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Task>
 */
class TaskFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            //
            'Title' => fake()->title(),
            'Description' => fake() -> text(100),
            'Deadline' => fake() -> dateTimeInInterval('+1 day', '+1 week'),
            'Status' => fake() -> boolean()
        ];
    }
}
