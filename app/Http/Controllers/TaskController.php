<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TaskController extends Controller
{
    //
    public function create()
    {

    }

    public function read($id)
    {
        $task = Task::findOrFail($id);
        return view('tasks.show',['task'=>$task]);
    }

    public function update()
    {

    }

    public function delete()
    {
        Task::where('id', $id)->delete();
        return redirect()->back()->with('success', 'Delete successfully');
    }
}
